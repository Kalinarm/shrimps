using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BeatPhysics : MonoBehaviour
{
	public int beatModulo = 4;
	public int beatOffset = 0;
	public Rigidbody body;
	
	public Vector3 worldImpulse = Vector3.up;
	public ForceMode forceMode = ForceMode.VelocityChange;
	
	void OnEnable() {
		GameManager.Events.AddListener<Evt.IdleBeat>(onIdleBeat);
	}
	void OnDisable() {
		GameManager.Events.RemoveListener<Evt.IdleBeat>(onIdleBeat);
	}
	
	#region evt callback
	void onIdleBeat(Evt.IdleBeat evt) {
		if ((evt.beatNumber + beatOffset) % beatModulo != 0) return;
		if (body != null) {
			body.AddForce(worldImpulse, forceMode);
		}
	}
	#endregion
}
		