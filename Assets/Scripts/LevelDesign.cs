﻿using System;
using UnityEngine;

[CreateAssetMenu]
public class LevelDesign : ScriptableObject
{
	[Serializable]
	public class ElementInfo : WeightedItem
	{
		public int weight;
		public GameObject prefab;
		public string attackEventPath;

		public int getWeight()
		{
			return weight;
		}
	}

	public enum Kind
	{
		Poisson,
		Corail,
		TopToBottom,
	}

	[Serializable]
	public class ElementInfoSet
	{
		public Kind kind;

		public ElementInfo[] elementInfos;

		public float minSpawnDelay;
		public float maxSpawnDelay;

		public Vector2 speed;

		public int warmLoops;
	}

	public Sprite startSprite;

	public string[] ambianceEventPaths;

	public ElementInfoSet[] elementInfoSets;
}
