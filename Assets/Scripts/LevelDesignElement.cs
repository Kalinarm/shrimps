﻿using UnityEngine;

public class LevelDesignElement : MonoBehaviour
{
	[HideInInspector]
	public SpeedProvider speedProvider;

	[HideInInspector]
	public Vector2 speed;

	[HideInInspector]
	public Vector2 gameplayAreaLimits;

	private FMOD.Studio.EventInstance attackEventInstance;

	public string attackEventPath
	{
		set
		{
			if (value.Length > 0)
			{
				attackEventInstance = FMODUnity.RuntimeManager.CreateInstance(value);
			}
		}
	}

	public void succeedAttack()
	{
		if (attackEventInstance.isValid())
		{
			attackEventInstance.start();
		}
	}

	private void Update()
	{
		var currentSpeed = speed * speedProvider.getSpeedFactor();

		var position = transform.localPosition;
		position.x -= currentSpeed.x * Time.deltaTime;
		position.y -= currentSpeed.y * Time.deltaTime;
		transform.localPosition = position;

		if (Mathf.Abs(position.x) > gameplayAreaLimits.x || Mathf.Abs(position.y) > gameplayAreaLimits.y)
		{
			Destroy(gameObject);
		}
	}
}
