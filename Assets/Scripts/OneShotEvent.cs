﻿using UnityEngine;

public class OneShotEvent : MonoBehaviour
{
	public string eventPath;
	private FMOD.Studio.EventInstance eventInstance;

	private void Start()
	{
		eventInstance = FMODUnity.RuntimeManager.CreateInstance(eventPath);
	}

	public void start()
	{
		eventInstance.start();
	}
}
