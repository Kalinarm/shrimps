﻿using DG.Tweening;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
	public string eventPath;
	private FMOD.Studio.EventInstance eventInstance;

	private float debutMusiqueSmooth = 0f;
	private Tweener debutMusiqueTweener;

	public float decreaseDebutMusiqueDuration;
	public float increaseDebutMusiqueDuration;

	private int instrumentsTarget = 0;
	private float instrumentsSmooth = 0f;
	private Tweener instrumentsTweener;

	public float decreaseInstrumentsDuration;
	public float increaseInstrumentsDuration;

	private float failSmooth = 0f;
	private Tweener failTweener;

	public float decreaseFailDuration;
	public float increaseFailDuration;

	private void Start()
	{
		eventInstance = FMODUnity.RuntimeManager.CreateInstance(eventPath);
		eventInstance.start();
	}

	private void tweenDebutMusique(float target, float duration)
	{
		if (debutMusiqueTweener != null)
		{
			debutMusiqueTweener.Kill();
		}

		debutMusiqueTweener = DOTween.To(() =>
		{
			Debug.LogFormat("Starting transition: debut musique to {0}", target);
			return debutMusiqueSmooth;
		}, (value) =>
		{
			debutMusiqueSmooth = value;
			eventInstance.setParameterByName("DebutMusique", value);
		}, target, duration)
			.SetEase(Ease.Linear)
			.OnComplete(() =>
			{
				Debug.LogFormat("Ending transition: debut musique to {0}", target);
			});
	}

	public void startDebutMusique()
	{
		tweenDebutMusique(1f, increaseDebutMusiqueDuration);
	}

	public void stopDebutMusique()
	{
		tweenDebutMusique(0f, decreaseDebutMusiqueDuration);
	}

	private void tweenInstruments(int newInstruments, float duration)
	{
		if (instrumentsTweener != null)
		{
			instrumentsTweener.Kill();
		}

		instrumentsTarget = newInstruments;
		instrumentsTweener = DOTween.To(() =>
		{
			Debug.LogFormat("Starting transition: instruments to {0}", instrumentsTarget);
			return instrumentsSmooth;
		}, (value) =>
		{
			instrumentsSmooth = value;
			var actualValue = value;
			if (actualValue >= 5f)
			{
				actualValue += 1f;
			}
			eventInstance.setParameterByName("Instruments", actualValue);
		}, instrumentsTarget, duration)
			.SetEase(Ease.Linear)
			.OnComplete(() =>
		{
			Debug.LogFormat("Ending transition: instruments to {0}", instrumentsTarget);
		});
	}

	public void decreaseInstruments()
	{
		if (instrumentsTarget > 0)
		{
			tweenInstruments(instrumentsTarget - 1, decreaseInstrumentsDuration);
		}
	}

	public void increaseInstruments()
	{
		if (instrumentsTarget < 8)
		{
			tweenInstruments(instrumentsTarget + 1, increaseInstrumentsDuration);
		}
	}

	private void tweenFail(float target, float duration)
	{
		if (failTweener != null)
		{
			failTweener.Kill();
		}

		failTweener = DOTween.To(() =>
		{
			Debug.LogFormat("Starting transition: fail to {0}", target);
			return failSmooth;
		}, (value) =>
		{
			eventInstance.setParameterByName("Fail", value);
		}, target, duration)
			.SetEase(Ease.Linear)
			.OnComplete(() =>
			{
				Debug.LogFormat("Ending transition: fail to {0}", target);
			});
	}

	public void failStart()
	{
		tweenFail(1f, increaseFailDuration);
	}

	public void failStop()
	{
		tweenFail(0f, increaseFailDuration);
	}
}
