using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class FxTrigger
{
	public GameObject prefab;
	public float timeLive = 10f;
	public void trigger(Vector3 pos) {
		trigger(pos, Quaternion.identity);
	}
	
	public void trigger(Vector3 pos, Quaternion rot) {
		GameObject obj = GameObject.Instantiate(prefab);
		obj.transform.position = pos;
		obj.transform.rotation = rot;
		GameObject.Destroy(obj, timeLive);
	}
}
		