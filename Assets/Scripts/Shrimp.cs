using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Shrimp : MonoBehaviour
{
	public Animator animator;
	public Collider2D collider;
	public string triggerOnIdleBeat = "onBeat";
	
	public Vector3 forward = Vector3.right;
	
	public float forceAtDeath = 50f;
	public float forceRotateAtDeath = 50f;
	
	void OnEnable() {
		GameManager.Events.AddListener<Evt.IdleBeat>(onIdleBeat);
	}
	void OnDisable() {
		GameManager.Events.RemoveListener<Evt.IdleBeat>(onIdleBeat);
	}
	
	/*void OnCollisionEnter(Collision col) {
		Obstacle obs = col.gameObject.GetComponent<Obstacle>();
		if (obs != null) {
			onCollideWithObstacle(obs);
		}
	}
	void OnTriggerEnter(Collider other) {
		Obstacle obs = other.GetComponent<Obstacle>();
		if (obs != null) {
			onCollideWithObstacle(obs);
		}
	}*/
	
	void onCollideWithObstacle(Obstacle obs) {
		GameManager.Events.Trigger(new Evt.ShrimpCollision(this, obs));
	}
	
	public void executeSwarmAction(PlayerSwarm.ACTION action) {
		switch(action) {
			case PlayerSwarm.ACTION.MOVE_FORWARD:
				
			break;
			case PlayerSwarm.ACTION.MOVE_BACKWARD:

			break;
		}
	}
	
	public void activateRagdoll() {
		transform.SetParent(null);
		Rigidbody2D rigid = gameObject.AddComponent<Rigidbody2D>();
		rigid.AddForce(new Vector2(-5f, -3f) * forceAtDeath, ForceMode2D.Force);
		rigid.AddTorque(forceRotateAtDeath);
		if (animator != null) {
			animator.SetBool("dead", true);
		}
		Invoke("activateCollider", 4f);
		Invoke("autodestroy", 10f);
	}
	public void startMoveAnimation() {
		if (animator != null) {
			animator.SetTrigger("move");
		}
	}
	void activateCollider() {
		if (collider != null) {
			collider.enabled = true;
		}
	}
	void autodestroy() {
		GameObject.Destroy(gameObject);
	}
	#region evt callback
	void onIdleBeat(Evt.IdleBeat evt) {

	}
	#endregion
}
		