﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ScoreManager))]
public class ScoreManagerEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		var scoreManager = target as ScoreManager;

		if (GUILayout.Button("Succeed Combo"))
		{
			scoreManager.succeedCombo();
		}

		if (GUILayout.Button("Fail combo"))
		{
			scoreManager.failCombo();
		}
	}
}
