﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SpeedManager))]
public class SpeedManagerEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		var speedManager = target as SpeedManager;

		if (GUILayout.Button("Start"))
		{
			speedManager.start();
		}

		if (GUILayout.Button("Stop"))
		{
			speedManager.stop();
		}
	}
}
