﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelManager))]
public class LevelManagerEditor : Editor
{
	LevelDesign levelDesign;

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		var levelManager = target as LevelManager;

		levelDesign = EditorGUILayout.ObjectField("Level Design", levelDesign, typeof(LevelDesign)) as LevelDesign;

		if (levelDesign != null && GUILayout.Button("Set Level Design"))
		{
			levelManager.setCurrentLevelDesign(levelDesign);
		}
	}
}
