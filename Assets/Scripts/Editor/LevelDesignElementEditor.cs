﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelDesignElement))]
public class LevelDesignElementEditor : Editor
{
	LevelDesign levelDesign;

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		var levelDesignElement = target as LevelDesignElement;

		if (GUILayout.Button("Succeed Attack"))
		{
			levelDesignElement.succeedAttack();
		}
	}
}
