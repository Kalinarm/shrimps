﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MusicManager))]
public class MusicManagerEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		var musicManager = target as MusicManager;

		if (GUILayout.Button("Start Debut Musique"))
		{
			musicManager.startDebutMusique();
		}

		if (GUILayout.Button("Stop Debut Musique"))
		{
			musicManager.stopDebutMusique();
		}

		if (GUILayout.Button("Decrease Instruments"))
		{
			musicManager.decreaseInstruments();
		}

		if (GUILayout.Button("Increase Instruments"))
		{
			musicManager.increaseInstruments();
		}

		if (GUILayout.Button("Fail Start"))
		{
			musicManager.failStart();
		}

		if (GUILayout.Button("Fail Stop"))
		{
			musicManager.failStop();
		}
	}
}
