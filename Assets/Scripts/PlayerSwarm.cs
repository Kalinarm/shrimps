using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PlayerSwarm : MonoBehaviour
{
	public enum ACTION {
	UNDEFINED,
	STAY,
	MOVE_FORWARD,
	MOVE_BACKWARD,
	MOVE_UP,
	MOVE_DOWN
	}
	
	public Shrimp prefab;
	
	public int startInstanceCount = 7;
	public Vector2 spacingBetweenShrimp = Vector2.one;
	
	
	public Rigidbody2D rigid;
	
	public Vector2 moveVector = Vector3.right;
	public float moveDuration = 1f;
	public AnimationCurve moveCurve;
	
	public Vector2Int posGrid = Vector2Int.zero; 
	public Transform startPos;
	
	public List<Shrimp> shrimps = new List<Shrimp>();
	
	public List<Transform> shrimpPos = new List<Transform>();
	
	public FxTrigger fxCollide = new FxTrigger();
	
	public OneShotEvent fxMange;

	
	void Start() {
		rigid = GetComponent<Rigidbody2D>();
		prefab.gameObject.SetActive(false);
		createPrefabs();
	}
	
	void OnEnable() {
		GameManager.Events.AddListener<Evt.SwarmAction>(onSwarmAction);
		GameManager.Events.AddListener<Evt.ShrimpCollision>(onShrimpCollision);
	}
	
	void OnDisable() {
		GameManager.Events.RemoveListener<Evt.ShrimpCollision>(onShrimpCollision);
	}
	
	void Update() {
		//simulate actions
		if (Input.GetKeyDown(KeyCode.D)) {
			GameManager.Events.Trigger(new Evt.SwarmAction(ACTION.MOVE_FORWARD));
		}
		if (Input.GetKeyDown(KeyCode.Q)) {
			GameManager.Events.Trigger(new Evt.SwarmAction(ACTION.MOVE_BACKWARD));
		}
		if (Input.GetKeyDown(KeyCode.Z)) {
			GameManager.Events.Trigger(new Evt.SwarmAction(ACTION.MOVE_UP));
		}
		if (Input.GetKeyDown(KeyCode.S)) {
			GameManager.Events.Trigger(new Evt.SwarmAction(ACTION.MOVE_DOWN));
		}
	}
	
	void OnTriggerEnter(Collider other) {
		Obstacle obs = other.GetComponentInParent<Obstacle>();
		if (obs != null) {
			onCollideWithObstacle(obs);
			return;
		}
		
		Bonus bonus = other.GetComponentInParent<Bonus>();
		if (bonus != null) {
			onCollideWithBonus(bonus);
			return;
		}
	}
	void OnTriggerEnter2D(Collider2D other) {
		Obstacle obs = other.GetComponentInParent<Obstacle>();
		if (obs != null) {
			fxCollide.trigger((other.transform.position + transform.position)*0.5f);
			onCollideWithObstacle(obs);
			
			LevelDesignElement elem = obs.GetComponent<LevelDesignElement>();
			if (elem != null) {
				elem.succeedAttack();
			}
			return;
		}
		
		Bonus bonus = other.GetComponentInParent<Bonus>();
		if (bonus != null) {
			onCollideWithBonus(bonus);
			return;
		}
	}
	
	
	void onCollideWithObstacle(Obstacle obs) {
		Debug.Log("player swarm collision with obstacle");
		looseShrimp();
		if (fxMange != null) fxMange.start();
	}
	void onCollideWithBonus(Bonus obs) {
		Debug.Log("player swarm collision with bonus");
		winShrimp();
	}
	
	void looseShrimp() {
		if (shrimps.Count > 0) {
			Shrimp s = shrimps[shrimps.Count - 1];
			shrimps.RemoveAt(shrimps.Count - 1);
			s.activateRagdoll();
			//GameObject.Destroy(s.gameObject);
			if (shrimps.Count == 0) {
				looseGame();
			}
		}
	}
	
	public void winShrimp() {
		Debug.Log("create shrimp");
		if (shrimps.Count <7)
			createShrimpObj(shrimps.Count);
	}
	
	public void looseGame() {
		Debug.Log("loose game");
		GameManager.Instance.changeGameState(GAME_STATE.LOOSE, true);
	}
	
	void createPrefabs() {
		for (int i = 0 ; i<startInstanceCount ;++i) {
			createShrimpObj(i);
		}
	}
	
	void createShrimpObj(int i) {
			Shrimp s = GameObject.Instantiate(prefab, startPos);
			s.gameObject.SetActive(true);
			Vector3 p = new Vector3(-spacingBetweenShrimp.x * i, i % 2 == 0 ? -spacingBetweenShrimp.y : spacingBetweenShrimp.y, 0f );
			if (shrimpPos.Count > i) {
				p = shrimpPos[i].localPosition;
			}
			s.transform.localPosition = p;
			shrimps.Add(s);
			
	}
	
	void executeSwarmAction(ACTION action) {
		//Debug.Log("swarm action : " + action.ToString());
		foreach(Shrimp s in shrimps) {
			s.executeSwarmAction(action);
		}
		switch(action) {
			case PlayerSwarm.ACTION.MOVE_FORWARD:
				changePos(posGrid.x + 1, posGrid.y);
			break;
			case PlayerSwarm.ACTION.MOVE_BACKWARD:
				changePos(posGrid.x - 1, posGrid.y);
			break;
			case PlayerSwarm.ACTION.MOVE_UP:
				changePos(posGrid.x, posGrid.y + 1);
				
			break;
			case PlayerSwarm.ACTION.MOVE_DOWN:
				changePos(posGrid.x, posGrid.y - 1);
			break;
		}
	}
	
	void changePos(int x, int y) {
		posGrid.x = Mathf.Clamp(x, -1, 1);
		posGrid.y = Mathf.Clamp(y, -1, 1);
		moveTo(new Vector3(posGrid.x * moveVector.x, posGrid.y * moveVector.y, 0f));
	}
	
	void moveTo(Vector3 newPos) {
		StopAllCoroutines();
		StartCoroutine(moveRoutine(newPos, moveDuration));
		foreach(Shrimp s in shrimps) {
			s.startMoveAnimation();
		}
	}

	IEnumerator moveRoutine(Vector3 newPos, float duration) {
		Vector3 oldPos = transform.localPosition;
		WaitForEndOfFrame wait = new WaitForEndOfFrame();
		for (float f = 0f; f <= duration; f += Time.deltaTime) {
			float fCurve = moveCurve.Evaluate(f / duration);
			transform.localPosition = Vector3.Lerp(oldPos, newPos, fCurve);
			yield return wait;
		}
		transform.localPosition = newPos;
	}
	
	#region evt callback
	void onSwarmAction(Evt.SwarmAction evt) {
		
		executeSwarmAction(evt.action);
	}
	void onShrimpCollision(Evt.ShrimpCollision evt) {
		
	}
	#endregion
}
		