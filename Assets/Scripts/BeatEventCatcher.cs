using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

[System.Serializable]
public class BeatEvent : UnityEvent<int>
{
}
[System.Serializable]
public class SpecificBeatEvent : UnityEvent
{
}
public class BeatEventCatcher : MonoBehaviour
{
	public int beatModulo = 4;
	public int beatOffset = 0;
	public BeatEvent OnBeat;
	public SpecificBeatEvent onBeatBarBegin;
	public SpecificBeatEvent onBeatBarMid;
	
	void OnEnable() {
		GameManager.Events.AddListener<Evt.IdleBeat>(onIdleBeat);
	}
	void OnDisable() {
		GameManager.Events.RemoveListener<Evt.IdleBeat>(onIdleBeat);
	}
	
	#region evt callback
	void onIdleBeat(Evt.IdleBeat evt) {
		if (evt.beatCount == 0) {
			onBeatBarBegin.Invoke();
		}
		if (evt.beatCount == 2) {
			onBeatBarMid.Invoke();
		}
		if ((evt.beatNumber + beatOffset) % beatModulo != 0) return;
		OnBeat.Invoke(evt.beatCount);
	}
	#endregion
}