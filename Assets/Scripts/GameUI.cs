using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour
{
	public Image timerImage;
	public GameObject gamePanel;
	public GameObject loosePanel;
	public GameObject winPanel;

	public float timePanel = 4f;

	public Text texCombo;
	public Text texLevel;

	public List<GameObject> panels = new List<GameObject>();
	public List<GameObject> tutos = new List<GameObject>();

	public string bobineFilmEventpath;
	private FMOD.Studio.EventInstance bobineFilmEventInstance;
	public float bobineFilmDecreaseDebutMusiqueDuration;
	public float bobineFilmIncreaseDebutMusiqueDuration;

	void Start()
	{
		bobineFilmEventInstance = FMODUnity.RuntimeManager.CreateInstance(bobineFilmEventpath);
		bobineFilmEventInstance.setParameterByName("DebutMusique", 1f);
		bobineFilmEventInstance.start();

		GameManager.Events.AddListener<Evt.GameStateChanged>(onGameStateChanged);
		if (gamePanel != null) gamePanel.SetActive(true);
		if (loosePanel != null) loosePanel.SetActive(false);
		if (winPanel != null) winPanel.SetActive(false);

		StartCoroutine(introRoutine());
	}

	void Update()
	{
		if (timerImage != null)
		{
			timerImage.fillAmount = GameManager.Instance.CurrentTimeNormalized;
		}
		if (texCombo != null)
		{
			texCombo.text = GameManager.Instance.combo.ToString();
		}
		if (texLevel != null)
		{
			texLevel.text = GameManager.Instance.currentLevel.ToString();
		}
	}

	public void AskStartLevel()
	{

	}
	public void AskRestartLevel()
	{

	}

	public void setIntroPanel(int i)
	{
		foreach (var t in tutos) t.SetActive(false);
		if (i < 0 || i >= tutos.Count)
		{
			return;
		}
		tutos[i].SetActive(true);
	}

	private IEnumerator introRoutine()
	{
		DOTween.To(() => 1f, (value) => bobineFilmEventInstance.setParameterByName("DebutMusique", value), 0f, bobineFilmIncreaseDebutMusiqueDuration)
				.SetEase(Ease.Linear);


		foreach (var p in panels)
		{
			p.SetActive(false);
		}
		for (int u = 0; u < panels.Count; ++u)
		{
			panels[u].SetActive(true);
			yield return new WaitForSeconds(timePanel);
			panels[u].SetActive(false);
		}

		DOTween.To(() => 0f, (value) => bobineFilmEventInstance.setParameterByName("DebutMusique", value), 1f, bobineFilmDecreaseDebutMusiqueDuration)
				.SetEase(Ease.Linear);

	}

	#region evt callback
	void onGameStateChanged(Evt.GameStateChanged evt)
	{
		switch (evt.state)
		{
			case GAME_STATE.LOOSE:
				if (loosePanel != null) loosePanel.SetActive(true);
				break;
			case GAME_STATE.WIN:
				winPanel.SetActive(true);
				break;
		}
	}
	#endregion
}
