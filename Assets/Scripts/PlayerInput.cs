using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum PLAYER_ACTION {
	NOT_PRESSED,
	MISSED,
	A,
	B,
	C,
	D
}

[System.Serializable]
public class ActionPress {
	public PLAYER_ACTION action;
	public float timePress;
	public ActionPress() {}
	public ActionPress(PLAYER_ACTION _action, float time) {
		action = _action;
		timePress = time;
	}
}

[System.Serializable]
public class ActionsPress {
	public float margin = 0.1f;
	
	public List<ActionPress> actions = new List<ActionPress> ();
	public List<ActionPress> valids = new List<ActionPress> ();


	ActionPress LastAction {
		get {
			return actions[actions.Count -1];
		}
	}
	void deleteLastAction() {
		actions.RemoveAt(actions.Count -1);
	}
	public void createValids() {
		valids.Clear();
		for (int i =0; i < 4 ; ++i) {
			valids.Add(new ActionPress(PLAYER_ACTION.NOT_PRESSED, 0f));
		}
	}
	public void addAction(PLAYER_ACTION action, float time) {
		actions.Add(new ActionPress(action, time));
	}
	
	public void validate(int beatCount, float time) {
		if (actions.Count == 0) {
			valids[beatCount] = new ActionPress(PLAYER_ACTION.MISSED, time);
			//Debug.Log("press " + beatCount.ToString() + " : not pressed");
			return;
		}
		if (actions.Count > 0) {
			if (Mathf.Abs(LastAction.timePress - time) < margin) {
				valids[beatCount] = LastAction;
				//Debug.Log("press " + beatCount.ToString() + " : " + LastAction.action.ToString());
			} else {
				valids[beatCount] =  new ActionPress(PLAYER_ACTION.MISSED, time);
				//Debug.Log("press " + beatCount.ToString() + " : missed");
			}
		}
	}
	
	public bool isSameAs(List<PLAYER_ACTION> a) {
		for(int i = 0 ; i < a.Count ; ++i) {
			if (valids[i].action != a[i])
				return false;
		}
		return true;
	}
	
	public void resetActions() {
		actions.Clear();
		for (int i =0; i < 4 ; ++i) {
			valids[i] = (new ActionPress(PLAYER_ACTION.NOT_PRESSED, 0f));
		}
	}
}

public class PlayerInput : MonoBehaviour
{
	public ActionsPress actions = new ActionsPress();
	public string buttonName = "Fire";
	
	public KeyCode keyA = KeyCode.UpArrow;
	public KeyCode keyB = KeyCode.DownArrow;
	public KeyCode keyC = KeyCode.LeftArrow;
	public KeyCode keyD = KeyCode.RightArrow;
	
	public Evt.PlayerPress evtToLaunch = null;
	
	public List<PlayerButton> buttons = new List<PlayerButton>();
	
	public int actionsCount = 4;
	public List<PLAYER_ACTION> actionForward = new List<PLAYER_ACTION>();
	public List<PLAYER_ACTION> actionBackward = new List<PLAYER_ACTION>();
	public List<PLAYER_ACTION> actionUp = new List<PLAYER_ACTION>();
	public List<PLAYER_ACTION> actionDown = new List<PLAYER_ACTION>();
	
	public OneShotEvent fxFail;
	public OneShotEvent fxSucess;
	public OneShotEvent fxRestartSong;
	
	void Start() {
		actions.createValids();
		GameManager.Events.AddListener<Evt.IdleBeat>(onIdleBeat);
		GameManager.Events.AddListener<Evt.LateBeat>(onLateBeat);
	}
	void Update() {
		/*if (evtToLaunch != null && Input.GetButtonUp(buttonName)) {
			evtToLaunch.timeStop = GameManager.CurrentTime;
			GameManager.Events.Trigger(evtToLaunch);
			evtToLaunch = null;
		}
		if (evtToLaunch == null && Input.GetButtonDown(buttonName)) {
			evtToLaunch = new Evt.PlayerPress(0,GameManager.CurrentTime);
			GameManager.Events.Trigger(evtToLaunch);
		}*/
		
		if (Input.GetKeyDown(keyA)) {
			actions.addAction(PLAYER_ACTION.A, GameManager.CurrentTime);
		}
		if (Input.GetKeyDown(keyB)) {
			actions.addAction(PLAYER_ACTION.B, GameManager.CurrentTime);
		}
		if (Input.GetKeyDown(keyC)) {
			actions.addAction(PLAYER_ACTION.C, GameManager.CurrentTime);
		}
		if (Input.GetKeyDown(keyD)) {
			actions.addAction(PLAYER_ACTION.D, GameManager.CurrentTime);
		}
	}
	
	PlayerSwarm.ACTION getOrderFromPattern() {
		PlayerSwarm.ACTION r = PlayerSwarm.ACTION.UNDEFINED;
		if (actions.isSameAs(actionForward)) {
			r = PlayerSwarm.ACTION.MOVE_FORWARD;
		}
		if (actions.isSameAs(actionBackward)) {
			r = PlayerSwarm.ACTION.MOVE_BACKWARD;
		}
		if (actions.isSameAs(actionUp)) {
			r = PlayerSwarm.ACTION.MOVE_UP;
		}
		if (actions.isSameAs(actionDown)) {
			r = PlayerSwarm.ACTION.MOVE_DOWN;
		}
		return r;
	} 
	
	void refreshButtons() {
		for(int i = 0 ;i < buttons.Count; ++i) {
			buttons[i].SetButton(actions.valids[i].action);
		}
	}
	void validateSequence() {
		PlayerSwarm.ACTION act = getOrderFromPattern();
		Debug.Log("order " + act.ToString());
		if (act == PlayerSwarm.ACTION.UNDEFINED) {
			GameManager.Instance.resetCombo();
			if (fxFail != null && GameManager.Instance.tutorial > 0) {
				fxFail.start();
				/*GameManager.Instance.music.failStart();
				Invoke("failStop", 1f);*/
			} 
			return;
		}
		GameManager.Events.Trigger(new Evt.SwarmAction(act));
		if (fxSucess != null) fxSucess.start();

	}
	void failStop() {
		GameManager.Instance.music.failStop();
		if(fxRestartSong != null)fxRestartSong.start();
	}
	void reset() {
		actions.resetActions();
		refreshButtons();
	}
	
	#region evt callback
	void onIdleBeat(Evt.IdleBeat evt) {
		buttons[evt.beatCount].triggerBeat();
	}
	void onLateBeat(Evt.LateBeat evt) {
		/*if (evt.beatCount == 0) {
			reset();
		}*/
		actions.validate(evt.beatCount % actionsCount, evt.currentTime);
		if (evt.beatCount == actionsCount - 1) {
			validateSequence();
			Invoke("reset", 0.315f);
		}
		refreshButtons();
	}
	#endregion
	
}
		