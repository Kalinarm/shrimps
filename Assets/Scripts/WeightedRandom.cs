﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine.Assertions;

public interface WeightedItem
{
	int getWeight();
}


public static class WeightedRandom
{
	public static T pick<T>(IEnumerable<T> items) where T : WeightedItem
	{
		var totalWeight = items.Select(item => item.getWeight()).Sum();
		var pickedWeight = UnityEngine.Random.Range(0, totalWeight);

		Assert.IsTrue(totalWeight > 0, "Total weighted should be greater than 0.");

		var pickedItem = items.FirstOrDefault(item =>
		{
			if (pickedWeight < item.getWeight())
			{
				return true;
			}

			pickedWeight -= item.getWeight();
			return false;
		});

		return pickedItem;
	}
}
