﻿using DG.Tweening;
using UnityEngine;

public interface SpeedProvider
{
	float getSpeedFactor();
}

public class SpeedManager : MonoBehaviour, SpeedProvider
{
	[SerializeField]
	[Range(0f, 1f)]
	private float speedFactor = 1f;

	public float increaseDuration;
	public float decreaseDuration;

	private Tweener tweener;

	private void tween(float target, float duration)
	{
		if (tweener != null)
		{
			tweener.Kill();
		}

		tweener = DOTween.To(() => speedFactor, (value) => { speedFactor = value; }, target, duration)
			.SetEase(Ease.Linear);
	}

	public void start()
	{
		tween(1f, increaseDuration);
	}

	public void stop()
	{
		tween(0f, decreaseDuration);
	}

	public float getSpeedFactor()
	{
		return speedFactor;
	}
}
