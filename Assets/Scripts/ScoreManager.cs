﻿using System.Collections;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
	public MusicManager musicManager;

	public int successCountToIncreaseInstruments = 1;
	private int currentSuccessCount = 0;
	public string successEventPath;
	private FMOD.Studio.EventInstance successEventInstance;

	public int failCountToDecreaseInstruments = 1;
	private int currentFailCount = 0;
	public string failEventPath;
	private FMOD.Studio.EventInstance failEventInstance;

	public float failDuration;
	public string restartSongEventPath;
	private FMOD.Studio.EventInstance restartSongEventInstance;

	private void Start()
	{
		successEventInstance = FMODUnity.RuntimeManager.CreateInstance(successEventPath);
		failEventInstance = FMODUnity.RuntimeManager.CreateInstance(failEventPath);
		restartSongEventInstance = FMODUnity.RuntimeManager.CreateInstance(restartSongEventPath);
	}

	public void succeedCombo()
	{
		currentFailCount = 0;
		++currentSuccessCount;
		successEventInstance.start();

		if (currentSuccessCount >= successCountToIncreaseInstruments)
		{
			currentSuccessCount = 0;
			musicManager.increaseInstruments();
		}
	}

	public void failCombo()
	{
		currentSuccessCount = 0;
		++currentFailCount;
		failEventInstance.start();
		musicManager.failStart();

		if (currentFailCount >= failCountToDecreaseInstruments)
		{
			currentFailCount = 0;
			musicManager.decreaseInstruments();
		}

		StartCoroutine(waitAndStopFail());
	}

	private IEnumerator waitAndStopFail()
	{
		yield return new WaitForSeconds(failDuration);
		musicManager.failStop();
		restartSongEventInstance.start();
	}
}
