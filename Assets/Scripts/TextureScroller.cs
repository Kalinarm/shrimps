﻿using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class TextureScroller : MonoBehaviour
{
	public SpeedManager speedProvider;

	public Vector2 speed;

	void Update()
	{
		var currentSpeed = speed * speedProvider.getSpeedFactor();

		var renderer = GetComponent<Renderer>();
		var offset = renderer.material.mainTextureOffset;
		offset += currentSpeed * Time.deltaTime;
		renderer.material.mainTextureOffset = offset;
	}
}
