using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BeatAnim : MonoBehaviour
{
	public int beatModulo = 4;
	public int beatOffset = 0;
	
	public float duration = 0.5f;
	public AnimationCurve curve = AnimationCurve.Constant(0f,1f,1f);
	public Vector3 scale = Vector3.one;
	
	public Transform target;
	
	void OnEnable() {
		if (target == null) {
			target = transform;
		}
		scale = target.localScale;
		GameManager.Events.AddListener<Evt.IdleBeat>(onIdleBeat);
	}
	void OnDisable() {
		GameManager.Events.RemoveListener<Evt.IdleBeat>(onIdleBeat);
	}
	IEnumerator routine() {
		WaitForEndOfFrame wait = new WaitForEndOfFrame();
		float fNorm ;
		for(float f = 0f; f <duration; f+=Time.deltaTime) {
			fNorm = curve.Evaluate(f/duration);
			target.localScale = fNorm * scale;
			yield return wait;
		}
	}
	#region evt callback
	void onIdleBeat(Evt.IdleBeat evt) {
		StopAllCoroutines();
		StartCoroutine(routine() );
	}
	#endregion
}
		