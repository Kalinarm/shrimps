using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Obstacle : MonoBehaviour
{
	void OnEnable() {
		GameManager.Events.AddListener<Evt.IdleBeat>(onIdleBeat);
	}
	void OnDisable() {
		GameManager.Events.RemoveListener<Evt.IdleBeat>(onIdleBeat);
	}
	
	#region evt callback
	void onIdleBeat(Evt.IdleBeat evt) {

	}
	#endregion
}
		