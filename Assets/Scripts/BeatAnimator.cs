using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BeatAnimator : MonoBehaviour
{
	public Animator animator;
	
	public string triggerOnIdleBeat = "beat";
	public int beatModulo = 4;
	public int beatOffset = 0;
	
	void OnEnable() {
		GameManager.Events.AddListener<Evt.IdleBeat>(onIdleBeat);
	}
	void OnDisable() {
		GameManager.Events.RemoveListener<Evt.IdleBeat>(onIdleBeat);
	}
	
	#region evt callback
	void onIdleBeat(Evt.IdleBeat evt) {
		if ((evt.beatNumber + beatOffset) % beatModulo != 0) return;
		if (animator != null) {
			animator.SetTrigger(triggerOnIdleBeat);
		}
	}
	#endregion
}
		