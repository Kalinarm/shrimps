using UnityEngine;
using System.Collections;
using System.Collections.Generic;
 using UnityEngine.SceneManagement;

public enum GAME_STATE{
	PLAYING,
	LOOSE,
	WIN
}

public class GameManager : MonoBehaviour
{
	public PlayerSwarm playerSwarm;
	public MusicManager music;
	public GameUI ui;
	
	public GAME_STATE currentState = GAME_STATE.PLAYING;
	public bool isPause = false;
	
	public bool startFakeRythm = false;
	public int bpm = 120;
	public int beatNumber = 0;
	public int tutorial = 0;
	
	float secPerBeat;
	float currentTime;
	float startTime;
	float lastTimeDSP;
	float lastTimePattern;
	
	static GameManager s_instance = null;
	public float timeStart = 4f;
	public int currentLevel = 0;
	public int combo = 0;
	public int comboLevel = 0;
	public LevelManager level;
	public List<LevelDesign> levelsSpawn = new List<LevelDesign> ();
	
	public List<PlayerSwarm.ACTION> tutoActions = new List<PlayerSwarm.ACTION> ();
	
	public static GameManager Instance {
		get {
			if (s_instance == null) {
				s_instance = GameObject.FindObjectOfType<GameManager>();
			}
			return s_instance;
		}
	}
	public static Core.EventManager Events {
		get {
			return GameManager.Instance.evtMgr;
		}
	}
	public static float CurrentTime {
		get {
			return (float)AudioSettings.dspTime - Instance.startTime;
		}
	}
	public float CurrentTimeNormalized {
		get {
			return (CurrentTime  - lastTimePattern)/ (4f * secPerBeat);
		}
	}
	
	public Core.EventManager evtMgr = new Core.EventManager();
	
	void Awake() {
		Debug.Log("register events");
		evtMgr.AddListener<Evt.PlayerPress>(onPlayerPress);
		evtMgr.AddListener<Evt.ShrimpCollision>(onShrimpCollision);
		evtMgr.AddListener<Evt.SwarmAction>(onSwarmAction);
	}
	
	void OnDestroy() {
		
	}
	
	void Start() {
		if (startFakeRythm) {
			StartCoroutine(fakeRythm());
		}
		StartCoroutine(introRoutine());
	}
	
	void Update() {
		evtMgr.doStep(Time.deltaTime);
		
		if (Input.GetKeyDown(KeyCode.Return)) {
			reloadScene();
		}
		if (Input.GetKeyDown(KeyCode.R)) {
			incrementLevel();
		}
	}
	
	private IEnumerator introRoutine()
	{
		yield return new WaitForSeconds(timeStart);
		currentLevel = -1;
		music.startDebutMusique();
		incrementLevel();
		ui.setIntroPanel(tutorial);
	}
	public void resetCombo() {
		combo = 0;
		comboLevel = 0;
		if (tutorial > 6) {
			music.decreaseInstruments();
		}
	}
	public void incrementCombo() {
		combo++;
		if (currentLevel != 0)comboLevel++;
		music.increaseInstruments();
		if (comboLevel >= 4) {
			incrementLevel();
			comboLevel = 0;
		}
	}
	public void reloadScene() {
		 SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}
	void incrementLevel() {

		currentLevel++;
		if (currentLevel >= levelsSpawn.Count) {
			changeGameState(GAME_STATE.WIN, true);
			return;
		}
		level.setCurrentLevelDesign(levelsSpawn[currentLevel]);
	}
	#region helper
	public void changeGameState(GAME_STATE newState, bool pause) {
		currentState = newState;
		isPause = pause;
		evtMgr.Trigger(new Evt.GameStateChanged(currentState, isPause));
	}
	#endregion
	
	#region evt callback
	void onPlayerPress(Evt.PlayerPress evt) {
		Debug.Log("player press " + evt.timeStart);
	}
	void onShrimpCollision(Evt.ShrimpCollision evt) {
		
	}
	void onSwarmAction(Evt.SwarmAction evt) {
		incrementCombo();
		if (tutorial > 6 ) {
			if (currentLevel == 0) {
				incrementLevel();
				return;
			}
		}
		if (tutoActions.Count == 0) {
			ui.setIntroPanel(++tutorial);
			return;
		}
		if (evt.action == tutoActions[0]) {
			tutoActions.RemoveAt(0);
			playerSwarm.winShrimp();
			ui.setIntroPanel(++tutorial);
		}
	}
	#endregion
	
	IEnumerator fakeRythm() {
		WaitForEndOfFrame wait = new WaitForEndOfFrame();
		
		//lastTimePattern = (float)AudioSettings.dspTime;
		
		//calculate how many seconds is one beat
		secPerBeat = 60f / (float)bpm;
		
		//yield return new WaitForSeconds(4f * secPerBeat);
		
		float lastTime = (float)AudioSettings.dspTime;
		lastTimePattern = (float)AudioSettings.dspTime;
		lastTimeDSP = (float)AudioSettings.dspTime;
		currentTime = (float)AudioSettings.dspTime;
		startTime = currentTime;
		OnBeat(currentTime);
		
		while(true) {
			float songTime = (float)AudioSettings.dspTime - startTime;
			float dt = (float)AudioSettings.dspTime - lastTimeDSP;
			lastTimeDSP = (float)AudioSettings.dspTime;
			
			currentTime = songTime;
			
			//if (currentTime - lastTime >= secPerBeat) {
			if (songTime - beatNumber * secPerBeat >= secPerBeat) {
				OnBeat(currentTime);
				lastTime = currentTime;
				yield return new WaitForSeconds(0.25f);
				evtMgr.Trigger(new Evt.LateBeat(beatNumber,currentTime));
			}
			yield return wait;
		}
	}
	
	void OnBeat(float currentTime) {
		//Debug.Log("beat");
		beatNumber++;
		evtMgr.Trigger(new Evt.IdleBeat(beatNumber,currentTime));
		if (beatNumber % 4 == 0) {
			lastTimePattern = currentTime;
		}
	}
	
	/*float mod(float a,float b)
	{
     return a - b * floor(a / b);
	}*/
	
	/*IEnumerator fakeRythm() {
		WaitForEndOfFrame wait = new WaitForEndOfFrame();
		
		//lastTimePattern = (float)AudioSettings.dspTime;
		
		//calculate how many seconds is one beat
		secPerBeat = 60f / (float)bpm;
		
		//yield return new WaitForSeconds(4f * secPerBeat);
		
		float lastTime = (float)AudioSettings.dspTime;
		lastTimePattern = (float)AudioSettings.dspTime;
		
		currentTime = (float)AudioSettings.dspTime;
		OnBeat(currentTime);
		
		while(true) {
			currentTime = (float)AudioSettings.dspTime;
			if (currentTime - lastTime >= secPerBeat) {
				OnBeat(currentTime);
				lastTime = currentTime;
				yield return wait;
				yield return wait;
				evtMgr.Trigger(new Evt.LateBeat(beatNumber,currentTime));
			}
			yield return wait;
			
		}
	}*/
	
	
}
		