using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Evt {
	
	public class GameStateChanged : IEvent
	{
		public GAME_STATE state;
		public bool isPause = false;
		public GameStateChanged(GAME_STATE _state, bool pause)
		{
			state = _state;
			isPause = pause;
		}
	}
	
	public class IdleBeat : IEvent
	{
		public int beatNumber = 0;
		public int beatCount {
			get {
				return beatNumber%4;
			}
		}
		public float currentTime;
		
		public IdleBeat(int _beatNumber, float _currentTime)
		{
			beatNumber = _beatNumber;
			currentTime = _currentTime;
		}
	}
	public class LateBeat : IEvent
	{
		public int beatNumber = 0;
		public int beatCount {
			get {
				return beatNumber%4;
			}
		}
		public float currentTime;
		
		public LateBeat(int _beatNumber, float _currentTime)
		{
			beatNumber = _beatNumber;
			currentTime = _currentTime;
		}
	}
	
	public class SwarmAction : IEvent
	{
		public PlayerSwarm.ACTION action;
		public SwarmAction(PlayerSwarm.ACTION _action)
		{
			action = _action;
		}
	}
	
	public class ShrimpCollision : IEvent
	{
		public Shrimp shrimp;
		public Obstacle obstacle;
		public ShrimpCollision(Shrimp s, Obstacle o)
		{
			shrimp = s;
			obstacle = o;
		}
	}
	
	
	public class PlayerPress : IEvent
	{
		public int button;
		public float timeStart;
		public float m_timeStop;
		public float timeStop {
			get {
				return m_timeStop;
			}
			set {
				m_timeStop = value;
				isRelease = true;
			}
		}
		public bool isRelease = false;
		
		public PlayerPress(int _button, float _timeStart)
		{
			button = _button;
			timeStart = _timeStart;
			isRelease = false;
		}
		
		public PlayerPress(int _button, float _timeStart, float _timeStop)
		{
			button = _button;
			timeStart = _timeStart;
			m_timeStop = _timeStop;
			isRelease = true;
		}
	}
}
