﻿using UnityEngine;
using System.Collections;

namespace Koon
{
	namespace Rhytm
	{
		public class GameNote : MonoBehaviour
		{
			AudioSource audioSource;
			//public Util.Colorizer colorizer = new Koon.Util.Colorizer ();
			/*public Fx.FxModel fxOnCreate = new Koon.Fx.FxModel ();
			public Fx.FxModel fxOnTrigger = new Koon.Fx.FxModel ();
			public Fx.FxModel fxOnHit = new Koon.Fx.FxModel ();
			public Fx.FxModel fxOnMissed = new Koon.Fx.FxModel ();
			public Fx.FxModel fxOnDestroy = new Koon.Fx.FxModel ();*/
			// Use this for initialization
			void Start ()
			{
				audioSource = GetComponent<AudioSource> ();
				//fxOnCreate.trigger (transform.position, transform.rotation);
			}
			void OnDestroy() {
				//fxOnDestroy.trigger (transform.position, transform.rotation);
			}
			public void Trigger() {
				if (audioSource) audioSource.Play ();
				//fxOnTrigger.trigger (transform.position, transform.rotation);
			}
			public void Hit() {
				//fxOnHit.trigger (transform.position, transform.rotation);
				//colorizer.setColor (Color.green);
			}
			public void Miss() {
				//fxOnMissed.trigger (transform.position, transform.rotation);
				//colorizer.setColor (Color.red);
			}
		}
	}
}