﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Koon
{
	namespace Rhytm
	{
		namespace Evt
		{
			public class SongStart : IEvent
			{
				RhytmManager manager;

				public SongStart (RhytmManager _manager)
				{
					manager = _manager;
				}
			}

			public class SongStop : IEvent
			{
				RhytmManager manager;

				public SongStop (RhytmManager _manager)
				{
					manager = _manager;
				}
			}

			public class Beat : IEvent
			{
				RhytmManager manager;
				float time;
				float timeInBeat;

				public Beat (RhytmManager _manager, float _time, float _timeInBeat)
				{
					manager = _manager;
					time = _time;
					timeInBeat = _timeInBeat;
				}
			}

			public class NoteHitByPlayer : IEvent
			{
				public RhytmPlayer player;
				public GameNote note;

				public NoteHitByPlayer (RhytmPlayer _player, GameNote _note)
				{
					player = _player;
					note = _note;
				}
			}

			public class NoteMissByPlayer : NoteHitByPlayer
			{
				public NoteMissByPlayer (RhytmPlayer _player, GameNote _note)
					: base (_player, _note)
				{
				}
			}

			public class RhytmPlayerComboChanged : IEvent
			{
				public RhytmPlayer player;
				public List<Element> combo;

				public RhytmPlayerComboChanged (RhytmPlayer _player, List<Element> _combo)
				{
					player = _player;
					combo = new List<Element> (_combo);
				}
			}

			public class RhytmPlayerComboFinish : IEvent
			{
				public RhytmPlayer player;
				public List<Element> combo;

				public RhytmPlayerComboFinish (RhytmPlayer _player, List<Element> _combo)
				{
					player = _player;
					combo = new List<Element> (_combo);
				}
			}

		}
		public enum Element
		{
			NONE,
			EARTH,
			FIRE,
			WATER,
			WIND,
			ICE,
			STEAM,
			LIGHTING,
			LIFE,
		}

		public class RhytmPlayer : MonoBehaviour
		{

			public List<Element> combo = new List<Element> (4);
			public int beatByCombo = 8;
			int lastBeat = 0;
			public List<GameObject> ennemyprefab = new List<GameObject> ();
			public float distanceEnnemy = 1f;
			public RhytmManager manager;
			bool record = false;

			[System.Serializable]
			public class InputState
			{
				public KeyCode keyCode;
				float timeAtPress = 0f;
				float timeAtRelease = 0f;
				bool isPressed = false;

				public float TimeAtPress {
					get {
						return timeAtPress;
					}
				}

				public float TimeAtRelease {
					get {
						return timeAtRelease;
					}
				}

				public bool IsPressed {
					get {
						return isPressed;
					}
				}

				public bool IsKeyDown {
					get {
						return Input.GetKeyDown (keyCode);
					}
				}

				public void clearPress ()
				{
					isPressed = false;
				}

				public void update (float t)
				{
					if (Input.GetKeyDown (keyCode)) {
						timeAtPress = t;
						isPressed = true;
					} else if (Input.GetKeyUp (keyCode)) {
						timeAtRelease = t;
						isPressed = false;
					}
				}
			}

			public float minThreshold = 0.5f;
			public float maxThreshold = 0.5f;
			public InputState stateA = new InputState ();
			public InputState stateB = new InputState ();
			public InputState stateC = new InputState ();

			void Start ()
			{
				manager.onNote += OnNote;
				manager.onNoteCreation += OnNoteEarly;
				manager.onBeat += OnBeat;

				//App.get.Events.Trigger (new UI.Evt.SetPanelVisible (UI.UIPanel.PANEL_TYPE.GAME_OUTRO, false));
				//App.get.Events.AddListener<Evt.SongStart> (onSongStart);
				//App.get.Events.AddListener<Evt.SongStop> (onSongStop);
			}


			void Update ()
			{
				stateA.update (manager.SongPosition);
				stateB.update (manager.SongPosition);
				stateC.update (manager.SongPosition);
			}

			void clearCombo ()
			{
				combo.Clear ();
			}

			void addCombo (Note n)
			{
				switch (n.type) {
				case Note.NOTE_TYPE.A:
					combo.Add (Element.EARTH);
					break;
				case Note.NOTE_TYPE.B:
					combo.Add (Element.FIRE);
					break;
				case Note.NOTE_TYPE.C:
					combo.Add (Element.WATER);
					break;
				}
				//App.get.Events.Trigger (new Evt.RhytmPlayerComboChanged (this, combo));
			}

			void onComboFinish ()
			{
				//App.get.Events.Trigger (new Evt.RhytmPlayerComboFinish (this, combo));
			}

			public void OnBeat (float time, float timeInBeat)
			{
				//App.get.Events.Trigger (new Evt.Beat (manager, time, timeInBeat));
				if ((int)timeInBeat >= (int)lastBeat + beatByCombo) {
					lastBeat = (int)timeInBeat;
					if (combo.Count > 0) {
						onComboFinish ();
					}
					clearCombo ();
				}

				if (Random.Range (0, 11) > 5) {
					GameObject obj = GameObject.Instantiate (ennemyprefab [0], manager.transform);
					obj.transform.position = Quaternion.Euler (0f, 4f * Mathf.Sin (Mathf.PI * Time.time), 0f) * Vector3.forward * (distanceEnnemy + Random.Range (-10f, 10f));
				}
			}

			public void OnNoteEarly (float time, Note n, NoteSpawn ns, GameObject prefab)
			{
				
			}

			public void OnNote (float time, Note n, GameObject prefab)
			{
				StartCoroutine (createNoteRoutine (time, n, prefab, maxThreshold));
			}

			void onSongStart (Evt.SongStart evt)
			{
				//App.get.Events.Trigger (new UI.Evt.SetPanelVisible (UI.UIPanel.PANEL_TYPE.GAME_INTRO, false));
				record = manager.record;
				StartCoroutine (recordRoutine ());
			}

			void onSongStop (Evt.SongStop evt)
			{
				//App.get.Events.Trigger (new UI.Evt.SetPanelVisible (UI.UIPanel.PANEL_TYPE.GAME_OUTRO, true));
			}

			IEnumerator recordRoutine ()
			{
				WaitForEndOfFrame wait = new WaitForEndOfFrame ();
				while (record) {
					checkIfRecordNote (Note.NOTE_TYPE.A);
					checkIfRecordNote (Note.NOTE_TYPE.B);
					checkIfRecordNote (Note.NOTE_TYPE.C);
					yield return wait;
				}
			}

			void checkIfRecordNote (Note.NOTE_TYPE type)
			{
				InputState input = findInput (type);
				if (input.IsPressed) {
					input.clearPress ();
					Note n = new Note ();
					n.time = manager.songPosInBeats;
					n.type = type;
					manager.songInfo.notes.Add (n);
				}
			}

			IEnumerator createNoteRoutine (float time, Note n, GameObject prefab, float delay)
			{
				yield return new WaitForSeconds (delay);
				InputState input = findInput (n.type);
				if (input == null)
					yield break;
				GameNote note = prefab.GetComponent<GameNote> ();

				if (input.TimeAtPress >= time - minThreshold
				    && (input.IsPressed || input.TimeAtRelease <= time + maxThreshold)) {
					Debug.Log ("hit");
					note.Hit ();
					manager.setMix (1f);
					addCombo (n);
					//App.get.Events.Trigger (new Evt.NoteHitByPlayer (this, note));
				} else {
					Debug.Log ("miss");
					note.Miss ();
					manager.setMix (0.0f);
					//App.get.Events.Trigger (new Evt.NoteMissByPlayer (this, note));
				}

			}

			InputState findInput (Note.NOTE_TYPE type)
			{
				switch (type) {
				case Note.NOTE_TYPE.A:
					return stateA;
				case Note.NOTE_TYPE.B:
					return stateB;
				case Note.NOTE_TYPE.C:
					return stateC;
				default :
					return stateA;
				}
			}
		}
	}
}
