﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Koon
{
	namespace Rhytm
	{
		[CreateAssetMenu]
		public class SongInfo : ScriptableObject
		{
			public string gameName;

			public AudioClip clip;
			//beats per minute of a song
			public float bpm = 100f;
			public float songOffset = 0f;
			//keep all the position-in-beats of notes in the song
			public List<Note> notes = new List<Note>();

			string defaultPath { get { return Application.dataPath + "/../"+gameName+".json"; } }

			public void orderNote() {
				notes.OrderBy (x => x.time);
			}
			public void deserialize(string s) {
				JsonUtility.FromJsonOverwrite (s, this);
				orderNote ();
			}
			public string serialize() {
				string s = JsonUtility.ToJson (this, true);
				return s;
			}
			public void deserializeFromFile (string path) {
				/*string s = Koon.Util.FileUtil.readFromFile (path);
				deserialize (s);*/
			}
			public void serializeToFile (string path) {
				/*string s = serialize();
				Koon.Util.FileUtil.exportStringToFile (path, s);*/
			}



			[ContextMenu("Save Song")]
			public void saveSong() {
				serializeToFile (defaultPath);
			}
			[ContextMenu("Load song")]
			public void loadSong() {
				deserializeFromFile (defaultPath);
			}
		}
	}
}
