﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Koon
{
	namespace Rhytm
	{
		public class RhytmManager : MonoBehaviour
		{
			public bool playOnStart = true;
			public SongInfo songInfo;
			public int beatsShownInAdvance = 0;
			public float beatFactor = 1f;

			public List<NoteSpawn> notesSpawns = new List<NoteSpawn> ();

			public Vector3 spawnPosition = Vector3.zero;
			public Vector3 PositionByTime = Vector3.zero;

			//the current position of the song (in seconds)
			public float songPosition;

			//the current position of the song (in beats)
			public float songPosInBeats;

			//the duration of a beat
			float secPerBeat;

			//how much time (in seconds) has passed since the song started
			float dsptimesong;

			//the index of the next note to be spawned
			int nextIndex = 0;

			float lastPosInBeat = 0f;

			public bool record = false;


			public delegate void OnBeat (float time, float timeInBeat);

			public OnBeat onBeat = null;

			public delegate void OnNote (float time, Note n, GameObject prefab);

			public OnNote onNote = null;

			public delegate void OnNoteCreation (float time, Note n, NoteSpawn ns, GameObject prefab);

			public OnNoteCreation onNoteCreation = null;

			public AudioSource audioSource;
			public AudioSource audioSource2;

			public float volumeSpeed = 10f;
			float currentVolume = 0f;
			float targetVolume = 0f;

			bool isPlaying = false;

			public float SongPosition {
				get {
					return songPosition;
				}
			}

			void Start ()
			{
				if (playOnStart) {
					Play ();
				} else {
					//GameManager.Events.AddListener<Game.Evt.GameLaunch> (onGameLaunch);
				}

			}

			/*void onGameLaunch (Game.Evt.GameLaunch evt)
			{
				Play ();
			}*/

			void Play ()
			{
				//calculate how many seconds is one beat
				secPerBeat = 60f / songInfo.bpm;

				//record the time when the song starts
				dsptimesong = (float)AudioSettings.dspTime;

				//start the song
				audioSource = GetComponent<AudioSource> ();
				if (songInfo.clip != null) {
					audioSource.clip = songInfo.clip;
				}
				audioSource.Play ();
				if (audioSource2 != null) {
					audioSource2.clip = songInfo.clip;
					audioSource2.Play ();
				}
				Invoke ("endOfSong", audioSource.clip.length + 1f);
				GameManager.Events.Trigger (new Evt.SongStart (this));

				isPlaying = true;
			}

			void Update ()
			{
				if (!isPlaying)
					return;
				
				if (audioSource2 != null) {
					currentVolume += (targetVolume - currentVolume) * Time.deltaTime * volumeSpeed * 0.1f;
					audioSource2.volume = currentVolume;
				} else {
					currentVolume += (targetVolume - currentVolume) * Time.deltaTime * volumeSpeed * 0.1f;
					audioSource.volume = currentVolume;
				}

				//calculate the position in seconds
				songPosition = (float)(AudioSettings.dspTime - dsptimesong) + songInfo.songOffset;

				//calculate the position in beats
				songPosInBeats = songPosition / secPerBeat;

				if (onBeat != null && Mathf.CeilToInt (songPosInBeats) >= Mathf.CeilToInt (lastPosInBeat) + 2) {
					lastPosInBeat = songPosInBeats;
					onBeat (SongPosition, songPosInBeats);
				}

				transform.localPosition = -PositionByTime * songPosition;

				if (nextIndex < songInfo.notes.Count && songInfo.notes [nextIndex].time < songPosInBeats * beatFactor + beatsShownInAdvance) {
					CreateNoteEarly (songInfo.notes [nextIndex]);
					nextIndex++;
				}
			}

			void endOfSong ()
			{
				//App.get.Events.Enqueue (new Evt.SongStop (this));
			}

			void CreateNoteEarly (Note note)
			{
				NoteSpawn ns = findSpawn (note);
				if (ns == null)
					return;

				//Debug.Log ("play note");
				GameObject obj = CreapteSpawn (ns);
				if (onNoteCreation != null) {
					onNoteCreation (songPosition, note, ns, obj);
				}
				StartCoroutine (createNoteRoutine (songPosition, note, obj, beatsShownInAdvance * secPerBeat));
			}

			public void setMix (float value)
			{
				if (audioSource2 != null) {
					currentVolume = value;
					targetVolume = currentVolume * 0.8f;
				} else {
					targetVolume = value;
					currentVolume = targetVolume;
				}

			}

			IEnumerator createNoteRoutine (float songPosition, Note n, GameObject prefab, float delay)
			{
				yield return new WaitForSeconds (delay);
				if (onNote != null) {
					onNote (songPosition + delay, n, prefab);
				}
			}

			GameObject CreapteSpawn (NoteSpawn spawn)
			{
				if (spawn == null || spawn.prefab == null) {
					return null;
				}
				GameObject obj = GameObject.Instantiate (spawn.prefab, transform);
				obj.transform.localPosition = Vector3.Scale (PositionByTime, (songPosition + beatsShownInAdvance * secPerBeat) * Vector3.one) + spawnPosition + spawn.offset;
				GameNote note = obj.GetComponent<GameNote> ();
				if (note == null) {
					note = obj.AddComponent<GameNote> ();
				}
				note.Invoke ("Trigger", beatsShownInAdvance * secPerBeat);

				return obj;
			}

			NoteSpawn findSpawn (Note note)
			{
				return notesSpawns.Find (x => x.type == note.type);
			}

			[ContextMenu ("Save Song")]
			public void saveSong ()
			{
				songInfo.serializeToFile (Application.dataPath + "/../song.json");
			}

			[ContextMenu ("Load song")]
			public void loadSong ()
			{
				songInfo.deserializeFromFile (Application.dataPath + "/../song.json");
			}
		}
	}
}
