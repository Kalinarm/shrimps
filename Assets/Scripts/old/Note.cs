﻿using UnityEngine;
using System.Collections;

namespace Koon
{
	namespace Rhytm
	{
		[System.Serializable]
		public class Note
		{
			public enum NOTE_TYPE {
				A,
				B,
				C,
				D
			}
			public float time = 0f;
			public NOTE_TYPE type = NOTE_TYPE.A;
		}

		[System.Serializable]
		public class NoteSpawn
		{
			public Note.NOTE_TYPE type = Note.NOTE_TYPE.A;
			public GameObject prefab;
			public AudioClip soundSuccess;
			public Vector3 offset = Vector3.zero;
		}
	}
}
