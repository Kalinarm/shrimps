using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerButton : MonoBehaviour
{
	public int index = 0;
	public GameObject buttonMissed;
	public GameObject buttonA;
	public GameObject buttonB;
	public GameObject buttonC;
	public GameObject buttonD;
	
	public Animator animator;
	
	void Start() {
		SetButton(PLAYER_ACTION.NOT_PRESSED);
	}
	public void SetButton(PLAYER_ACTION action) {
		if (buttonA != null) buttonA.SetActive(false);
		if (buttonB != null) buttonB.SetActive(false);
		if (buttonC != null) buttonC.SetActive(false);
		if (buttonD != null) buttonD.SetActive(false);
		if (buttonMissed != null) buttonMissed.SetActive(false);
		switch(action) {
			case PLAYER_ACTION.A : if (buttonA != null) buttonA.SetActive(true);break;
			case PLAYER_ACTION.B : if (buttonB != null) buttonB.SetActive(true);break;
			case PLAYER_ACTION.C : if (buttonC != null) buttonC.SetActive(true);break;
			case PLAYER_ACTION.D : if (buttonD != null) buttonD.SetActive(true);break;
			case PLAYER_ACTION.MISSED : if (buttonMissed != null) buttonMissed.SetActive(true);break;
		}
	}
	
	public void triggerBeat() {
		if (animator != null) {
			animator.SetTrigger("beat");
		}
	}
}
		