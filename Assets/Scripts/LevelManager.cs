﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
	public SpeedManager speedProvider;

	private LevelDesign currentLevelDesign;

	public Vector2 gameplayAreaLimits;

	public Transform[] rightSpawnTransforms;

	public Transform corailTopSpawnTransform;
	public Transform corailBottomSpawnTransform;

	public Transform topLeftSpawnTransform;
	public Transform topRightSpawnTransform;

	private List<Tuple<FMOD.Studio.EventInstance, Tweener>> ambianceEventInstances = new List<Tuple<FMOD.Studio.EventInstance, Tweener>>();
	public float decreaseAmbianceVolumeDuration;
	public float increaseAmbianceVolumeDuration;

	public void setCurrentLevelDesign(LevelDesign levelDesign)
	{
		StopAllCoroutines();

		foreach (var ambianceEventInstance in ambianceEventInstances)
		{
			ambianceEventInstance.Item2.Kill();

			DOTween.To(() => 1f, (value) => ambianceEventInstance.Item1.setVolume(value), 0f, decreaseAmbianceVolumeDuration)
				.SetEase(Ease.Linear)
				.OnComplete(() =>
				{
					ambianceEventInstance.Item1.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
				});
		}
		ambianceEventInstances.Clear();

		currentLevelDesign = levelDesign;

		foreach (var ambianceEventPath in currentLevelDesign.ambianceEventPaths)
		{
			var ambianceEventInstance = FMODUnity.RuntimeManager.CreateInstance(ambianceEventPath);
			ambianceEventInstance.start();

			var tweener = DOTween.To(() => 0f, (value) => ambianceEventInstance.setVolume(value), 1f, increaseAmbianceVolumeDuration)
				.SetEase(Ease.Linear) as Tweener;

			ambianceEventInstances.Add(Tuple.Create(ambianceEventInstance, tweener));
		}

		foreach (var elementInfoSet in levelDesign.elementInfoSets)
		{
			float delay = 0f;

			for (var i = 0; i < elementInfoSet.warmLoops; ++i)
			{
				delay += UnityEngine.Random.Range(elementInfoSet.minSpawnDelay, elementInfoSet.maxSpawnDelay);
				spawn(levelDesign, elementInfoSet, delay);
			}

			StartCoroutine(spawnAndWait(levelDesign, elementInfoSet));
		}
	}

	private IEnumerator spawnAndWait(LevelDesign levelDesign, LevelDesign.ElementInfoSet elementInfoSet)
	{
		if (currentLevelDesign == levelDesign)
		{
			spawn(levelDesign, elementInfoSet);

			var spawnDelay = UnityEngine.Random.Range(elementInfoSet.minSpawnDelay, elementInfoSet.maxSpawnDelay);
			yield return new WaitForSeconds(spawnDelay);

			StartCoroutine(spawnAndWait(levelDesign, elementInfoSet));
		}
	}

	private void spawn(LevelDesign levelDesign, LevelDesign.ElementInfoSet elementInfoSet, float delay = 0f)
	{
		var elementInfo = WeightedRandom.pick(elementInfoSet.elementInfos);

		if (elementInfo.prefab == null)
		{
			return;
		}

		var instance = Instantiate(elementInfo.prefab);

		var ldElement = instance.GetComponent<LevelDesignElement>();
		ldElement.attackEventPath = elementInfo.attackEventPath;
		ldElement.speedProvider = speedProvider;
		ldElement.speed = elementInfoSet.speed;
		ldElement.gameplayAreaLimits = gameplayAreaLimits;

		switch (elementInfoSet.kind)
		{
			case LevelDesign.Kind.Poisson:
				{
					var transformIndex = UnityEngine.Random.Range(0, rightSpawnTransforms.Length);
					var spawnTransform = rightSpawnTransforms[transformIndex];
					var position = spawnTransform.position;
					position.x -= delay * elementInfoSet.speed.x;
					instance.transform.position = position;
					break;
				}

			case LevelDesign.Kind.Corail:
				{
					var position = Vector3.LerpUnclamped(corailTopSpawnTransform.position, corailBottomSpawnTransform.position, UnityEngine.Random.value);
					position.x -= delay * elementInfoSet.speed.x;
					instance.transform.position = position;
					break;
				}

			case LevelDesign.Kind.TopToBottom:
				{
					var position = Vector3.LerpUnclamped(topLeftSpawnTransform.position, topRightSpawnTransform.position, UnityEngine.Random.value);
					position.y -= delay * elementInfoSet.speed.y;
					instance.transform.position = position;
					break;
				}
		}
	}
}
